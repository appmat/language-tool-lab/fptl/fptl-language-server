# FPTL Language Server
***
Языковой сервер на Kotlin с поддержкой LSP для языка FPTL.

### Язык FPTL
[FPTL](https://github.com/Zumisha/FPTL "Информация о языке") (Functional 
Programming Typified Language) - функциональный язык для высокопроизводительных вычислений

### Используемые технологии
- [LSP4J](https://github.com/eclipse/lsp4j "О lsp4j"), реализация LSP для Java
- [ANTLR4](https://github.com/antlr/antlr4 "О antlr4"), генератор парсеров

### Плагин VSCode
Реализованы: подсветка синтаксиса, описание стандартных функций и предложение слова по префиксу.
Работает с файлами с расширением .fptl  
[Перейти](vs-extension)
