plugins {
    antlr
    jacoco
    application
    `maven-publish`
    kotlin("jvm") version "1.5.21"
    id("com.github.johnrengelman.shadow") version "7.0.0"
    id("org.jetbrains.dokka") version "1.5.0"
    id("io.gitlab.arturbosch.detekt") version "1.18.0-RC3"
}

group = "ru.appmat"

if (version == Project.DEFAULT_VERSION) {
    version = "1.0-SNAPSHOT"
}

val kotlin_version: String by project
val koin_version: String by project
val junit_version: String by project
val lsp4j_version: String by project
val gson_version: String by project
val antlr4_version: String by project

repositories {
    mavenCentral()
    maven(url = "https://gitlab.com/api/v4/projects/28787621/packages/maven")
}

dependencies {
    antlr("org.antlr:antlr4:$antlr4_version")

    implementation("org.eclipse.lsp4j:org.eclipse.lsp4j:$lsp4j_version")
    implementation("com.google.code.gson:gson:$gson_version")

    implementation("io.insert-koin:koin-core:$koin_version")
    testImplementation("io.insert-koin:koin-test:$koin_version")

    testImplementation("org.junit.jupiter:junit-jupiter:$junit_version")
    testImplementation("org.junit.jupiter:junit-jupiter-params:$junit_version")

    // detekt gitlab report
    detektPlugins("com.gitlab.cromefire:detekt-gitlab-report:0.2.0")
}

application {
    mainClass.set("ru.appmat.ltp.fptl.language_server.MainKt")
}

tasks {
    compileKotlin {
        dependsOn(generateGrammarSource)
    }
    test {
        useJUnitPlatform()
        finalizedBy(jacocoTestReport)
    }
    // https://gitlab.com/cromefire_/detekt-gitlab-report
    detekt {
        reports {
            custom {
                reportId = "DetektGitlabReport"
                // This tells detekt, where it should write the report to,
                // you have to specify this file in the gitlab pipeline config.
                destination = file("${project.buildDir}/reports/detekt/gitlab.json")
            }
        }
    }
    jacocoTestReport {
        reports {
            xml.required.set(true)
        }
    }
}

// https://github.com/Kotlin/dokka/blob/master/examples/gradle/dokka-library-publishing-example/build.gradle.kts
var dokkaJavadocJar = tasks.register<Jar>("dokkaJavadocJar") {
    dependsOn(tasks.dokkaJavadoc)
    from(tasks.dokkaJavadoc.flatMap { it.outputDirectory })
    archiveClassifier.set("javadoc")
}

publishing {
    publications {
        create<MavenPublication>("library") {
            from(components["java"])
            artifact(dokkaJavadocJar)
            artifact(tasks.kotlinSourcesJar)
        }
    }
    repositories {
        maven("${System.getenv("CI_API_V4_URL")}/projects/${System.getenv("CI_PROJECT_ID")}/packages/maven") {
            name = "GitLab"
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}
