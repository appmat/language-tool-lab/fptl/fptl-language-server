grammar Fptl;

@header {
package ru.appmat.ltp.fptl.language_server;
}
/*
Example
______________________________________________
Scheme Factorial
{
	@ = ([1] * 0).gequal ->
			((([1] * [1].toInt).sub * 0).equal ->
				Fact.print,
				true),
			true
		-> (("Ошибка: аргумент должен быть положительным целым числом! x = " * [1].toString).cat * "\n").cat.print;

	x = [1].toReal;

	Fact = (x * 0.0).equal -> 1.0, (x * (x * 1).sub.Fact).mul;
}

Application
% Factorial(100)
______________________________________________
*/

// Надо ли учитывать END_LINE? Все выражения заканчиваются на ; ,если я правильно понимаю
// Глава 7.2

program
    : scheme (application)?
    ;

scheme
    : SCHEME IDENTIFIER BEGIN_BLOCK announcement* END_BLOCK
    ;

application
    : APPLICATION call*
    ;

call
    : '%' IDENTIFIER function
    | '%' IDENTIFIER
    ;

announcement
    : name EQUAL function ';'
    ;

function
    : constant
    | function '.' name
    | function IF function (',' function)?
    | L_BRACKET function ('*' function)* R_BRACKET
    ;

name
    : identifier
    | THIS
    ;

constant
    : number
    | stringConstant
    | identifier
    | '[' DEC_NUMBER ']'
    ;

stringConstant: STRINT_CONSTANT;

identifier: IDENTIFIER;

number
    : realNumber
    | DEC_NUMBER
    ;

realNumber
    : FRAC EXP?
    | DEC_NUMBER EXP
    ;

DEC_NUMBER: '0' | ('-')?[1-9][0-9]*;
FRAC: ('-')?[0-9]*'.'[0-9]+;
EXP: ('-')?[+-]?[0-9]+;

// Я не уверен, что так надо
BEGIN_BLOCK: '{';
END_BLOCK: '}';
L_BRACKET: '(';
R_BRACKET: ')';
//'['
//']'
EQUAL: '=';
SCHEME: 'Scheme';
APPLICATION: 'Application';
THIS: '@';
IF: '->';


STRINT_CONSTANT: '"'.*?'"';
IDENTIFIER: [a-zA-Z][a-zA-Z0-9_]*;
//ENDLINE: '\n' -> skip;


COMMENT: '//'.*?'\n' -> skip;
COMMENT_BLOCK: '/*'.*?'*/' -> skip;
WS: [ \t\r\n]+ -> skip;
