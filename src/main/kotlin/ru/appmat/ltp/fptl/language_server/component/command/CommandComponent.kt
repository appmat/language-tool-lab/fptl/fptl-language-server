package ru.appmat.ltp.fptl.language_server.component.command

import ru.appmat.ltp.fptl.language_server.component.command.model.CommandParams
import java.util.concurrent.CompletableFuture

interface CommandComponent {
    fun executeCommand(params: CommandParams): CompletableFuture<Any>
}
