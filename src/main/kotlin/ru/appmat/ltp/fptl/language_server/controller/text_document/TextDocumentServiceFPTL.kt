package ru.appmat.ltp.fptl.language_server.controller.text_document

import org.eclipse.lsp4j.*
import org.eclipse.lsp4j.jsonrpc.messages.Either
import org.eclipse.lsp4j.services.TextDocumentService
import ru.appmat.ltp.fptl.language_server.component.completion.CompletionComponent
import ru.appmat.ltp.fptl.language_server.component.files.FilesComponent
import ru.appmat.ltp.fptl.language_server.component.hover.HoverComponent
import java.util.concurrent.CompletableFuture


class TextDocumentServiceFPTL(
    private val componentCompletion: CompletionComponent,
    private val componentHover: HoverComponent,
    private val componentFiles: FilesComponent
) : TextDocumentService {

    override fun didOpen(params: DidOpenTextDocumentParams?) {
        if (params != null) {
            componentFiles.addFile(params.textDocument.uri, params.textDocument.text)
        }
    }

    override fun didChange(params: DidChangeTextDocumentParams?) {
        if (params != null) {
            componentFiles.addFile(params.textDocument.uri, params.contentChanges[0].text)
        }
    }

    override fun didClose(params: DidCloseTextDocumentParams?) {
        if (params != null) {
            componentFiles.removeFile(params.textDocument.uri)
        }
    }

    override fun didSave(params: DidSaveTextDocumentParams?) {

    }

    override fun hover(params: HoverParams?): CompletableFuture<Hover>? {
        return CompletableFuture.completedFuture(componentHover.hover(params?.textDocument?.uri, params?.position))
    }

    override fun completion(position: CompletionParams?): CompletableFuture<Either<MutableList<CompletionItem>, CompletionList>> {
        return CompletableFuture.completedFuture(
            Either.forRight(
                CompletionList(
                    componentCompletion.completion(
                        position?.textDocument?.uri ?: ""
                    )
                )
            )
        )
    }

    override fun resolveCompletionItem(unresolved: CompletionItem?): CompletableFuture<CompletionItem> {
        return CompletableFuture.completedFuture(unresolved)
    }

    override fun documentColor(params: DocumentColorParams?): CompletableFuture<MutableList<ColorInformation>> {
        return CompletableFuture.completedFuture(mutableListOf())
    }

    override fun colorPresentation(params: ColorPresentationParams?): CompletableFuture<MutableList<ColorPresentation>> {
        return CompletableFuture.completedFuture(mutableListOf())
    }
}
