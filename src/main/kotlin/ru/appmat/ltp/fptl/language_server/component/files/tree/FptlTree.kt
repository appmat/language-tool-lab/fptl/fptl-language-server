package ru.appmat.ltp.fptl.language_server.component.files.tree

import org.antlr.v4.runtime.CharStreams
import org.antlr.v4.runtime.CommonTokenStream
import org.eclipse.lsp4j.ColorInformation
import org.eclipse.lsp4j.CompletionItem
import ru.appmat.ltp.fptl.language_server.FptlLexer
import ru.appmat.ltp.fptl.language_server.FptlParser

class FptlTree(text: String) {
    private val mTreeListener: FptlTreeListener = FptlTreeListener()
    private val mText: String = text
    private val mStrings: List<String> = mText.lines()
    private val mProgram: FptlParser.ProgramContext

    init {
        val lexer = FptlLexer(CharStreams.fromString(mText))
        val parser = FptlParser(CommonTokenStream(lexer))
        parser.addParseListener(mTreeListener)
        mProgram = parser.program()
    }

    fun getColorList(): MutableList<ColorInformation> {
        return mTreeListener.getColorList()
    }

    fun getCompletionList(): MutableList<CompletionItem> {
        return mTreeListener.getCompletionList()
    }

    fun getText(): String {
        return mText
    }

    fun getStrings(): List<String> {
        return mStrings
    }
}
