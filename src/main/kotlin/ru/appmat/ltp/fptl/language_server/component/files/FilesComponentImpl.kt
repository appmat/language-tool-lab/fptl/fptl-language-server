package ru.appmat.ltp.fptl.language_server.component.files

import ru.appmat.ltp.fptl.language_server.component.files.tree.FptlTree

class FilesComponentImpl : FilesComponent {
    private val _files = mutableMapOf<String, FptlTree>()
    override val files: Map<String, FptlTree>
        get() = _files

    override fun addFile(uri: String, text: String) {
        _files[uri] = FptlTree(text)
    }

    override fun removeFile(uri: String) {
        _files.remove(uri)
    }
}
