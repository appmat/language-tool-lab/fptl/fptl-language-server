package ru.appmat.ltp.fptl.language_server.component.hover

import org.eclipse.lsp4j.Hover
import org.eclipse.lsp4j.Position

interface HoverComponent {
    fun hover(uri: String?, position: Position?): Hover?
}
