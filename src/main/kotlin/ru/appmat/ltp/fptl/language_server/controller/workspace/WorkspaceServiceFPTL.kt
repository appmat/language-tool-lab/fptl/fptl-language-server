package ru.appmat.ltp.fptl.language_server.controller.workspace

import org.eclipse.lsp4j.DidChangeConfigurationParams
import org.eclipse.lsp4j.DidChangeWatchedFilesParams
import org.eclipse.lsp4j.ExecuteCommandParams
import org.eclipse.lsp4j.jsonrpc.messages.Either
import org.eclipse.lsp4j.services.WorkspaceService
import ru.appmat.ltp.fptl.language_server.component.command.CommandComponent
import ru.appmat.ltp.fptl.language_server.component.command.model.CommandParams
import ru.appmat.ltp.fptl.language_server.utills.KotlinEither
import java.util.concurrent.CompletableFuture

class WorkspaceServiceFPTL(private val componentCommand: CommandComponent) : WorkspaceService {

    override fun didChangeConfiguration(params: DidChangeConfigurationParams?) {
        TODO("Not yet implemented")
    }

    override fun didChangeWatchedFiles(params: DidChangeWatchedFilesParams?) {
        TODO("Not yet implemented")
    }

    override fun executeCommand(params: ExecuteCommandParams?): CompletableFuture<Any> {
        requireNotNull(params) { "params should not be null" }
        return componentCommand.executeCommand(params.convert())
    }

    private fun ExecuteCommandParams.convert(): CommandParams {
        return CommandParams(
            command = this.command ?: "",
            params = this.arguments.orEmpty(),
            onProgress = { kotlinEither: KotlinEither<String, Int> ->
                this.workDoneToken = when (kotlinEither) {
                    is KotlinEither.Left -> Either.forLeft(kotlinEither.value)
                    is KotlinEither.Right -> Either.forRight(kotlinEither.value)
                }
            }
        )
    }

}
