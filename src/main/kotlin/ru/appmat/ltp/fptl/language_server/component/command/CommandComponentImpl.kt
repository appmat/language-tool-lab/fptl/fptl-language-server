package ru.appmat.ltp.fptl.language_server.component.command

import com.google.gson.JsonArray
import ru.appmat.ltp.fptl.language_server.component.command.model.CommandParams
import ru.appmat.ltp.fptl.language_server.component.command.model.Commands
import java.io.File
import java.util.concurrent.CompletableFuture

class CommandComponentImpl : CommandComponent {
    class CommandException(text: String = "Command exception") : Exception(text)

    override fun executeCommand(params: CommandParams): CompletableFuture<Any> {
        return when (params.command) {
            Commands.COMPILE.command -> {
                compile(params)
            }
            else -> throw CommandException("Unsupported command")
        }
    }

    private fun compile(params: CommandParams): CompletableFuture<Any> {
        try {
            val args = params.params[0] as JsonArray
            require(args.size() >= 3) { "require 3 or more parameters in compile command" }
            val pathFile = args[0].asJsonPrimitive.asString
            val folderWithFptl = args[1].asJsonPrimitive.asString
            val folderWithFilesIO = args[2].asJsonPrimitive.asString
            val proc = ProcessBuilder("$folderWithFptl\\fptl.exe", pathFile)
            if (args.size() > 3 && args[3].asJsonPrimitive.asString == "true") {
                proc.redirectInput(File("$folderWithFilesIO\\in.txt"))
            }
            proc.redirectOutput(File("$folderWithFilesIO\\out.txt"))
            proc.start()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return CompletableFuture.completedFuture(null)
    }
}
