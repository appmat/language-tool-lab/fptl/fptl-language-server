package ru.appmat.ltp.fptl.language_server.model

data class CompletionsJson(val completion: List<CompletionJson>)
data class CompletionJson(val name: String, val info: String = "", val tags: List<String>? = null)
