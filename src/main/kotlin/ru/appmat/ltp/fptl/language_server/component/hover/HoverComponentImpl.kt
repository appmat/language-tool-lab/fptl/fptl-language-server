package ru.appmat.ltp.fptl.language_server.component.hover

import org.eclipse.lsp4j.Hover
import org.eclipse.lsp4j.MarkupContent
import org.eclipse.lsp4j.Position
import ru.appmat.ltp.fptl.language_server.component.files.FilesComponent
import ru.appmat.ltp.fptl.language_server.model.CompletionListFPTL
import ru.appmat.ltp.fptl.language_server.utills.getWordFromString

class HoverComponentImpl(
    private val componentFiles: FilesComponent,
    // TODO Refactoring
    private val mCompletionListFPTL: CompletionListFPTL
) : HoverComponent {

    override fun hover(uri: String?, position: Position?): Hover? {
        // kind может быть либо "plaintext", либо "markdown"
        if (uri != null && position != null && componentFiles.files.containsKey(uri)) {
            val line = componentFiles.files[uri]!!.getStrings()[position.line]
            val word = getWordFromString(line, position.character)
            if (mCompletionListFPTL.hoverMap.containsKey(word)) {
                return Hover(
                    MarkupContent(
                        "plaintext",
                        mCompletionListFPTL.hoverMap[word]
                    )
                )
            }
        }
        return null
    }
}
