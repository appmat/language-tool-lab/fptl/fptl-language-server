package ru.appmat.ltp.fptl.language_server.component.completion

import org.eclipse.lsp4j.CompletionItem

interface CompletionComponent {
    fun completion(uri: String): MutableList<CompletionItem>
}
