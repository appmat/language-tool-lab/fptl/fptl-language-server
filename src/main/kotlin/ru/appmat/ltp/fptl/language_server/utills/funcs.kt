package ru.appmat.ltp.fptl.language_server.utills

val SYMBOLS = listOf(' ', '.', '\t', '\n', '[', ']', '{', '}', ';', '(', ')', '*', ',')

fun getWordFromString(line: String, character: Int): String {
    var start = character
    var stop = character
    while (start > 0 && !SYMBOLS.contains(line[start - 1])) {
        start--
    }
    while (stop < line.length && !SYMBOLS.contains(line[stop])) {
        stop++
    }
    return line.subSequence(start, stop).toString()
}
