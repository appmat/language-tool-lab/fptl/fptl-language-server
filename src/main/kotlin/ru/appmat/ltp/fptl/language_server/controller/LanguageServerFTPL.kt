package ru.appmat.ltp.fptl.language_server.controller

import org.eclipse.lsp4j.*
import org.eclipse.lsp4j.jsonrpc.messages.Either
import org.eclipse.lsp4j.services.*
import ru.appmat.ltp.fptl.language_server.component.command.model.Commands
import ru.appmat.ltp.fptl.language_server.controller.text_document.TextDocumentServiceFPTL
import ru.appmat.ltp.fptl.language_server.controller.workspace.WorkspaceServiceFPTL
import java.util.concurrent.CompletableFuture


class LanguageServerFTPL(
    private val textDocumentServiceFPTL: TextDocumentServiceFPTL,
    private val workspaceService: WorkspaceServiceFPTL
) : LanguageServer, LanguageClientAware {
    private var client: LanguageClient? = null
    private var workspaceFolders = mutableListOf<String>()

    override fun shutdown(): CompletableFuture<Any> {
        return CompletableFuture.completedFuture(null)
    }

    override fun getTextDocumentService(): TextDocumentService {
        return textDocumentServiceFPTL
    }

    @Suppress("EmptyFunctionBlock")
    override fun exit() {
    }

    override fun initialize(params: InitializeParams?): CompletableFuture<InitializeResult> {
        params?.workspaceFolders?.forEach {
            workspaceFolders.add(it.uri)
        }
        val capabilities = ServerCapabilities()
        capabilities.setTextDocumentSync(TextDocumentSyncKind.Full)
        capabilities.setCodeActionProvider(false)
        capabilities.executeCommandProvider = ExecuteCommandOptions(listOf(Commands.COMPILE.command))
        capabilities.completionProvider = CompletionOptions(true, null)
        capabilities.setColorProvider(true)
        capabilities.hoverProvider = Either.forLeft(true)
        return CompletableFuture.completedFuture(InitializeResult(capabilities))
    }


    override fun getWorkspaceService(): WorkspaceService {
        return workspaceService
    }

    override fun connect(client: LanguageClient?) {
        this.client = client
    }
}
