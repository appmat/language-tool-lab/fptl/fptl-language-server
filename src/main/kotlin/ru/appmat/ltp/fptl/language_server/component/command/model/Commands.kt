package ru.appmat.ltp.fptl.language_server.component.command.model

enum class Commands(val command: String) {
    COMPILE("compile")
}
