package ru.appmat.ltp.fptl.language_server.component.command.model

import ru.appmat.ltp.fptl.language_server.utills.KotlinEither

class CommandParams(
    val command: String,
    val params: List<Any>,
    val onProgress: (KotlinEither<String, Int>) -> Unit
)
