package ru.appmat.ltp.fptl.language_server

import org.eclipse.lsp4j.launch.LSPLauncher
import org.koin.core.context.GlobalContext.get
import org.koin.core.context.startKoin
import ru.appmat.ltp.fptl.language_server.controller.LanguageServerFTPL
import ru.appmat.ltp.fptl.language_server.di.mainModule
import java.util.concurrent.Executors

fun main() {
    startKoin {
        modules(listOf(mainModule))
    }

    val inStream = System.`in`
    val outStream = System.out

    val server = get().get<LanguageServerFTPL>()
    val threads = Executors.newSingleThreadExecutor { Thread(it, "client") }
    val launcher = LSPLauncher.createServerLauncher(server, inStream, outStream, threads, { it })

    server.connect(launcher.remoteProxy)
    launcher.startListening()
}
