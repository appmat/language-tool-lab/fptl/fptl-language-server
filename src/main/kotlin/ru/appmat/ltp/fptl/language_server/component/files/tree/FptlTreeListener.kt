package ru.appmat.ltp.fptl.language_server.component.files.tree

import org.antlr.v4.runtime.misc.Interval
import org.eclipse.lsp4j.*
import ru.appmat.ltp.fptl.language_server.FptlBaseListener
import ru.appmat.ltp.fptl.language_server.FptlParser

class FptlTreeListener : FptlBaseListener() {
    private val mColorList = mutableListOf<ColorInformation>()
    private val mCompletionList = mutableListOf<CompletionItem>()

    override fun exitNumber(ctx: FptlParser.NumberContext?) {
        if (ctx != null) {
            val lastCharPositionInLine = ctx.start.charPositionInLine + (ctx.start.stopIndex - ctx.start.startIndex)
            mColorList.add(
                ColorInformation(
                    Range(
                        Position(ctx.start.line, ctx.start.charPositionInLine),
                        Position(ctx.start.line, lastCharPositionInLine)
                    ),
                    color1
                )
            )
        }
    }

    override fun exitStringConstant(ctx: FptlParser.StringConstantContext?) {
        if (ctx != null) {
            val lastCharPositionInLine = ctx.start.charPositionInLine + (ctx.start.stopIndex - ctx.start.startIndex)
            mColorList.add(
                ColorInformation(
                    Range(
                        Position(ctx.start.line, ctx.start.charPositionInLine),
                        Position(ctx.start.line, lastCharPositionInLine)
                    ),
                    color2
                )
            )
        }
    }

    override fun exitName(ctx: FptlParser.NameContext?) {
        if (ctx != null) {
            val lastCharPositionInLine = ctx.start.charPositionInLine + (ctx.start.stopIndex - ctx.start.startIndex)
            mColorList.add(
                ColorInformation(
                    Range(
                        Position(ctx.start.line, ctx.start.charPositionInLine),
                        Position(ctx.start.line, lastCharPositionInLine)
                    ),
                    color3
                )
            )

            val str = ctx.start.inputStream.getText(Interval(ctx.start.startIndex, ctx.start.stopIndex))
            for (x in mCompletionList) {
                if (x.label == str)
                    return
            }
            mCompletionList.add(CompletionItem(str).apply {
                kind = CompletionItemKind.Function
                detail = "in ${ctx.start.line} line"
            })
        }
    }

    fun getColorList(): MutableList<ColorInformation> {
        return mColorList
    }

    fun getCompletionList(): MutableList<CompletionItem> {
        return mCompletionList
    }

    companion object {
        private val color1 = Color(1.0, 0.4, 0.4, 1.0)
        private val color2 = Color(1.0, 0.9, 0.2, 1.0)
        private val color3 = Color(1.0, 0.8, 0.8, 1.0)
    }
}
