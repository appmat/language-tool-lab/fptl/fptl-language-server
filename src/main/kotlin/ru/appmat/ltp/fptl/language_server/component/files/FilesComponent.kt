package ru.appmat.ltp.fptl.language_server.component.files

import ru.appmat.ltp.fptl.language_server.component.files.tree.FptlTree

interface FilesComponent {
    val files: Map<String, FptlTree>

    fun addFile(uri: String, text: String)

    fun removeFile(uri: String)
}
