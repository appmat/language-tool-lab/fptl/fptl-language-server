package ru.appmat.ltp.fptl.language_server.component.completion

import org.eclipse.lsp4j.CompletionItem
import ru.appmat.ltp.fptl.language_server.component.files.FilesComponent
import ru.appmat.ltp.fptl.language_server.model.CompletionListFPTL

class CompletionComponentImpl(
    private val componentFiles: FilesComponent,
    // TODO Refactoring
    private val mCompletionListFPTL: CompletionListFPTL
) : CompletionComponent {

    override fun completion(uri: String): MutableList<CompletionItem> {
        val list = mCompletionListFPTL.completionList.toMutableList()
        val list0 = componentFiles.files[uri]?.getCompletionList().orEmpty()
        var i = 0
        var fl: Boolean
        while (i < list0.size) {
            fl = true
            var j = 0
            while (j < list.size && fl) {
                if (list0[i].label == list[j].label) {
                    fl = false
                }
                j++
            }
            if (fl) {
                list.add(list0[i])
            }
            i++
        }
        return list
    }
}
