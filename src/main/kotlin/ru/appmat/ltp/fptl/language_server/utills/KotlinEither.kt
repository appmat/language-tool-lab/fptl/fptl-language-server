package ru.appmat.ltp.fptl.language_server.utills

sealed class KotlinEither<out L, out R> {
    data class Left<out T>(val value: T) : KotlinEither<T, Nothing>()
    data class Right<out T>(val value: T) : KotlinEither<Nothing, T>()
}
