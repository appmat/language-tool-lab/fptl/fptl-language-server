package ru.appmat.ltp.fptl.language_server.model

import com.google.gson.Gson
import org.eclipse.lsp4j.CompletionItem
import org.eclipse.lsp4j.CompletionItemTag

class CompletionListFPTL {
    private val mCompletionList = mutableListOf<CompletionItem>()
    private val mHoverMap = mutableMapOf<String, String>()

    val completionList: List<CompletionItem> = mCompletionList
    val hoverMap: Map<String, String> = mHoverMap

    init {
        val file = CompletionListFPTL::class.java.getResource("/list.json")
        val elements = Gson().fromJson(file?.readText(), CompletionsJson::class.java)
        for (el in elements.completion) {
            mCompletionList.add(CompletionItem(el.name).apply {
                if (el.info != "") {
                    mHoverMap[el.name] = el.info
                    detail = el.info
                }
                if (!el.tags.isNullOrEmpty()) {
                    tags = el.tags.map { tag -> CompletionItemTag.valueOf(tag) }
                }
            })
        }
    }
}
