package ru.appmat.ltp.fptl.language_server.di

import org.koin.dsl.module
import org.koin.dsl.single
import ru.appmat.ltp.fptl.language_server.component.command.CommandComponent
import ru.appmat.ltp.fptl.language_server.component.command.CommandComponentImpl
import ru.appmat.ltp.fptl.language_server.component.completion.CompletionComponent
import ru.appmat.ltp.fptl.language_server.component.completion.CompletionComponentImpl
import ru.appmat.ltp.fptl.language_server.component.files.FilesComponent
import ru.appmat.ltp.fptl.language_server.component.files.FilesComponentImpl
import ru.appmat.ltp.fptl.language_server.component.hover.HoverComponent
import ru.appmat.ltp.fptl.language_server.component.hover.HoverComponentImpl
import ru.appmat.ltp.fptl.language_server.controller.LanguageServerFTPL
import ru.appmat.ltp.fptl.language_server.controller.text_document.TextDocumentServiceFPTL
import ru.appmat.ltp.fptl.language_server.controller.workspace.WorkspaceServiceFPTL
import ru.appmat.ltp.fptl.language_server.model.CompletionListFPTL

val mainModule = module {
    single<CompletionListFPTL>()

    single<CommandComponent> { CommandComponentImpl() }
    single<CompletionComponent> { CompletionComponentImpl(get(), get()) }
    single<FilesComponent> { FilesComponentImpl() }
    single<HoverComponent> { HoverComponentImpl(get(), get()) }

    factory { WorkspaceServiceFPTL(get()) }
    factory { TextDocumentServiceFPTL(get(), get(), get()) }
    factory { LanguageServerFTPL(get(), get()) }
}
