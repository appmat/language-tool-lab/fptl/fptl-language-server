package utils

import org.junit.jupiter.api.Test
import org.koin.test.KoinTest
import ru.appmat.ltp.fptl.language_server.utills.getWordFromString
import kotlin.test.assertEquals

class FuncsKtTest : KoinTest {

    @Test
    fun `в коде из одного слова успешно находим это слово`() {
        assertEquals("string", getWordFromString("string", 0))
    }
}
